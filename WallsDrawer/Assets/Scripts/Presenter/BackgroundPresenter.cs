﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class BackgroundPresenter : IBackgroundPresenter
{
    [Inject] private IAppPresenter _appPresenter;

    private IBackgroundView _backgroundView;

    public BackgroundPresenter(IBackgroundView backgroundView)
    {
        _backgroundView = backgroundView;
    }

    /// <summary>
    /// If you hit the background, then you need to put a point.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {
        _appPresenter.ClickPoint(eventData.position);
    }
}