using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class PointPresenter : IPointPresenter
{
    [Inject] private AppConfig _appConfig;
    [Inject] private IAppModel _appModel;
    [Inject] private IAppPresenter _appPresenter;

    private readonly IPointView _pointView;
    private Point _point;
    private bool _drag;

    public PointPresenter(IPointView pointView)
    {
        _pointView = pointView;
    }

    #region Implements

    public void Initialize(Point point)
    {
        _point = point;

        PlacedPoint(point);
        SubscribeOnChanges(point);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!_drag)
        {
            LoopingWall();
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _drag = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        DragPoint(eventData.position);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _drag = false;
    }

    #endregion

    private void SubscribeOnChanges(Point point)
    {
        point.OnPositionChanged += PlacedPoint;
    }

    private void PlacedPoint(Point point)
    {
        _pointView.Position = new Vector3(point.X, point.Y, _appConfig.DepthPosition);
    }

    private void LoopingWall()
    {
        _appPresenter.LoopingWall(_point);
    }

    private void DragPoint(Vector2 position)
    {
        var newPos = _appPresenter.ScreenToWorldPoint(position);

        if (_appModel.CanPointReplaceTo(newPos.x, newPos.y))
        {
            _point.SetNewPosition(newPos.x, newPos.y);
        }
    }
}