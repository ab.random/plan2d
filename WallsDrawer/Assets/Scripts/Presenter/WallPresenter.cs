using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class WallPresenter : IWallPresenter
{
    [Inject] private AppConfig _appConfig;
    [Inject] private IAppPresenter _appPresenter;

    private readonly IWallView _wallView;
    private Wall _wall;

    public Wall Wall => _wall;

    public bool Selected { get; private set; }

    public WallPresenter(IWallView wallView)
    {
        _wallView = wallView;
    }

    public void Initialize(Wall wall)
    {
        _wall = wall;

        ShowWall(wall);
        SubscribeOnChanges(wall);
    }

    public void Select(bool select)
    {
        Selected = select;
        _wallView.Select(Selected);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!Selected)
        {
            _appPresenter.Select(this);
        }
    }

    private void SubscribeOnChanges(Wall wall)
    {
        wall.OnThicknessChanged += wll => _wallView.Thickness = wll.Thickness;

        wall.Points.ForEach(point => point.OnPositionChanged += pnt => ShowWall(_wall));
    }

    private void ShowWall(Wall wall)
    {
        _wallView.Position = new Vector3(
            (wall.Points[0].X + wall.Points[1].X) / 2,
            (wall.Points[0].Y + wall.Points[1].Y) / 2,
            _appConfig.DepthPosition);

        var worldPoints = wall.Points.Select(point => new Vector3(point.X, point.Y, _appConfig.DepthPosition))
            .ToArray();
        _wallView.WorldPoints = worldPoints;

        _wallView.Length = Vector3.Distance(worldPoints[0], worldPoints[1]);

        // Angle from horizontal
        _wallView.PlaneRotation = Mathf.Atan2(
                                      wall.Points[1].Y - wall.Points[0].Y,
                                      wall.Points[1].X - wall.Points[0].X)
                                  * Mathf.Rad2Deg;

        _wallView.Thickness = wall.Thickness;
    }
}