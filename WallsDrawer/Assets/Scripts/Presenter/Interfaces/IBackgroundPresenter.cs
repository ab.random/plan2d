using UnityEngine.EventSystems;

public interface IBackgroundPresenter
{
    void OnPointerClick(PointerEventData eventData);
}