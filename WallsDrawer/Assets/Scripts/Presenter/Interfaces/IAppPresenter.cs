using UnityEngine;

public interface IAppPresenter
{
    bool HasSelected { get; }
    void ClickPoint(Vector2 position);
    void LoopingWall(Point point);
    Vector3 ScreenToWorldPoint(Vector2 screenPosition);
    void Select(ISelectable selectable);
    void Unselect();
}