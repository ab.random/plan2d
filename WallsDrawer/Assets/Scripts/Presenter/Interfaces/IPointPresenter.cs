using UnityEngine.EventSystems;

public interface IPointPresenter
{
    void Initialize(Point point);
    void OnPointerClick(PointerEventData eventData);
    void OnBeginDrag(PointerEventData eventData);
    void OnDrag(PointerEventData eventData);
    void OnEndDrag(PointerEventData eventData);
}