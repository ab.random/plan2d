public interface ISelectable
{
    bool Selected { get; }
    void Select(bool select);
}