using UnityEngine.EventSystems;

public interface IWallPresenter : ISelectable
{
    Wall Wall { get; }
    void Initialize(Wall wall);
    void OnPointerClick(PointerEventData eventData);
}