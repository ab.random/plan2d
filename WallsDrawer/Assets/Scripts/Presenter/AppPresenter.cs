using System.Linq;
using UnityEngine;
using Zenject;

public class AppPresenter : IAppPresenter
{
    [Inject] private Factories.Points.PointFactory _pointFactory;
    [Inject] private Factories.Points.PointViewFactory _pointViewFactory;
    [Inject] private Factories.Walls.WallFactory _wallFactory;
    [Inject] private Factories.Walls.WallViewFactory _wallViewFactory;
    [Inject] private Factories.Ui.WallPropertyViewFactory _wallPropertyViewFactory;

    [Inject] private Tags _tags;
    [Inject] private IAppModel _appModel;
    [Inject] private AppConfig _appConfig;
    [Inject] private IAppView _appView;

    private ISelectable _selectable;
    private IWallPropertyView _wallPropertyView;

    public bool HasSelected => _selectable != null;
    private float DeepOfDrawing => _appConfig.DepthPosition - Camera.main.transform.position.z;

    public void ClickPoint(Vector2 position)
    {
        if (HasSelected)
        {
            Unselect();
        }
        else
        {
            CreatePoint(position);
        }
    }

    public void LoopingWall(Point point)
    {
        if (_appModel.CanLoopWall(point))
        {
            var newWall = CreateWallView(point, _appModel.Points.Last());
            if (newWall != null)
            {
                _appModel.MakeLast(point);
            }
        }
    }

    public Vector3 ScreenToWorldPoint(Vector2 screenPosition)
    {
        return Camera.main.ScreenToWorldPoint(new Vector3(screenPosition.x, screenPosition.y, DeepOfDrawing));
    }

    public void Select(ISelectable selectable)
    {
        if (HasSelected)
        {
            Unselect();
        }

        _selectable = selectable;
        _selectable.Select(true);

        _wallPropertyView = _wallPropertyViewFactory.Create(((WallPresenter) selectable).Wall);
    }

    public void Unselect()
    {
        if (_selectable != null)
        {
            _selectable.Select(false);
            _selectable = null;
        }

        _wallPropertyView?.DestroyView();
    }

    private void CreatePoint(Vector2 position)
    {
        var pointView = CreatePointView(position);

        if (_appModel.CanWall())
        {
            CreateWallView(pointView.Point);
        }
    }

    private IPointView CreatePointView(Vector2 position)
    {
        Vector3 worldPos = ScreenToWorldPoint(position);

        Point point = _pointFactory.Create(worldPos.x, worldPos.y);
        _appModel.AddPoint(point);

        return _pointViewFactory.Create(point);
    }

    private IWallView CreateWallView(Point lastPoint)
    {
        var preLastPoint = _appModel.Points[_appModel.Points.Count - 2];
        return CreateWallView(lastPoint, preLastPoint);
    }

    private IWallView CreateWallView(Point point0, Point point1)
    {
        var wall = _wallFactory.Create(point0, point1, _appConfig.DefaultWallThickness);
        if (_appModel.Walls.Contains(wall)) return null;

        _appModel.AddWall(wall);
        return _wallViewFactory.Create(wall);
    }
}