﻿using UnityEngine;

[CreateAssetMenu(fileName = "AppConfig", menuName = "Project/AppConfig")]
public class AppConfig : ScriptableObject
{
    [SerializeField] private int _depthPosition = 1;
    [SerializeField] private float _wallThickness = 0.2f;
    [SerializeField] private float _stepThicknessChanging = 0.05f;
    [SerializeField] private float _minThickness = 0.01f;
    [SerializeField] private float _maxThickness = 1f;
    [Header("Prefabs")]
    [SerializeField] private GameObject _ui;
    [SerializeField] private GameObject _background;
    [SerializeField] private GameObject _point;
    [SerializeField] private GameObject _wall;

    public int DepthPosition => _depthPosition;
    public float DefaultWallThickness => _wallThickness;
    public float StepThicknessChanging => _stepThicknessChanging;
    public float MinThickness => _minThickness;
    public float MaxThickness => _maxThickness;

    public GameObject Ui => _ui;
    public GameObject Background => _background;
    public GameObject Point => _point;
    public GameObject Wall => _wall;
}