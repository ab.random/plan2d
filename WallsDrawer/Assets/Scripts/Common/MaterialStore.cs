using System;
using UnityEngine;

[CreateAssetMenu(fileName = "MaterialStore", menuName = "Project/Material store")]
public class MaterialStore : ScriptableObject
{
    [Serializable]
    private struct WallMaterials
    {
        public Material _defaultMaterial;
        public Material _selectedMaterial;
        public Material _aimedMaterial;
    }
    
    [Serializable]
    private struct PointMaterials
    {
        public Material _defaultMaterial;
        public Material _aimedMaterial;
    }

    [SerializeField] private WallMaterials _wallMaterials;
    [SerializeField] private PointMaterials _pointMaterials;
    
    public Material WallDefault => _wallMaterials._defaultMaterial;
    public Material WallSelected => _wallMaterials._selectedMaterial;
    public Material WallAimed => _wallMaterials._aimedMaterial;
    
    public Material PointDefault => _pointMaterials._defaultMaterial;
    public Material PointAimed => _pointMaterials._aimedMaterial;
}