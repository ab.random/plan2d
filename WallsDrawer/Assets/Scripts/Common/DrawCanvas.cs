﻿using UnityEngine;

public class DrawCanvas : MonoBehaviour
{
    [HideInInspector] [SerializeField] private Transform _cachedTransform;

    public Transform CachedTransform => _cachedTransform;

    #region Editor

    private void OnValidate()
    {
        if (!_cachedTransform)
        {
            _cachedTransform = transform;
        }
    }

    #endregion
}