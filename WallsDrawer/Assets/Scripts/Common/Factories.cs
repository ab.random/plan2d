using System;
using UnityEngine;
using Zenject;

public class Factories
{
    public class Points
    {
        public class PointFactory : PlaceholderFactory<float, float, Point>
        {
        }

        public class PointViewFactory : PlaceholderFactory<Point, PointView>
        {
        }

        public class PointPresenterFactory : PlaceholderFactory<IPointView, PointPresenter>
        {
        }
    }

    public class Walls
    {
        public class WallFactory : PlaceholderFactory<Point, Point, float, Wall>
        {
        }

        public class WallViewFactory : PlaceholderFactory<Wall, WallView>
        {
        }

        public class WallPresenterFactory : PlaceholderFactory<IWallView, WallPresenter>
        {
        }
    }

    public class Ui
    {
        public class BackgroundPresenterFactory : PlaceholderFactory<IBackgroundView, BackgroundPresenter>
        {
        }

        public class WallPropertyPresenterFactory : PlaceholderFactory<WallPropertyView, WallPropertyPresenter>
        {
        }

        public class WallPropertyViewFactory : PlaceholderFactory<Wall, WallPropertyView>
        {
        }
    }
}