using UnityEngine;

[CreateAssetMenu(fileName = "Tags", menuName = "Project/Tags")]
public class Tags : ScriptableObject
{
    [SerializeField] private TagString _pointTag;
    [SerializeField] private TagString _wallTag;

    public string PointTag => _pointTag.tag;
    public string WallTag => _wallTag.tag;
}