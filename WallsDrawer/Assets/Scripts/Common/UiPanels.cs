﻿using UnityEngine;

[CreateAssetMenu(fileName = "UiPanels", menuName = "Project/UI panels")]
public class UiPanels : ScriptableObject
{
    [SerializeField] private GameObject _wallPropertyPanel;

    public GameObject WallPropertyPanel => _wallPropertyPanel;
}