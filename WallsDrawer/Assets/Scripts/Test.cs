﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class Test : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("Pointer enter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("Pointer exit");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Point clicked");
    }
}