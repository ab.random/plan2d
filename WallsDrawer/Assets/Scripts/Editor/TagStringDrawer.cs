using System;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomPropertyDrawer(typeof(TagString))]
public class TagStringDrawer : PropertyDrawer
{
    private const int UNTAGGED_INDEX = 0;
    
    private static readonly string[] _tags = InternalEditorUtility.tags;
    private static readonly GUIContent[] _tagsGUIContents = Array.ConvertAll(_tags, i => new GUIContent(i));

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        property = property.FindPropertyRelative(nameof(TagString.tag));
        if (string.IsNullOrEmpty(property.stringValue))
        {
            property.stringValue = _tags[UNTAGGED_INDEX];
        }

        EditorGUI.BeginChangeCheck();
        int selectedIndex = Array.IndexOf(_tags, property.stringValue);
        selectedIndex = EditorGUI.Popup(position, label, selectedIndex, _tagsGUIContents);
        if (EditorGUI.EndChangeCheck())
        {
            property.stringValue = _tags[selectedIndex];
        }
    }
}