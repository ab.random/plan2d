﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class WallPropertyView : MonoBehaviour, IWallPropertyView
{
    [Inject] private Factories.Ui.WallPropertyPresenterFactory _wallPropertyPresenterFactory;

    [SerializeField] private Text _name;
    [SerializeField] private InputField _inputValue;
    [SerializeField] private Button _upValueButton;
    [SerializeField] private Button _downValueButton;
    [SerializeField] private Button _closeButton;

    [Inject] private Wall _wall;
    [Inject] private IAppView _appView;

    private IWallPropertyPresenter _wallPropertyPresenter;

    public String Name
    {
        get => _name.text;
        set => _name.text = value;
    }

    public float InputValue
    {
        get => float.Parse(_inputValue.text, NumberStyles.Float, CultureInfo.InvariantCulture);
        set => _inputValue.text = value.ToString(CultureInfo.InvariantCulture);
    }

    public InputField.SubmitEvent EndInputValueEvent => _inputValue.onEndEdit;
    public Button.ButtonClickedEvent UpValueButton => _upValueButton.onClick;
    public Button.ButtonClickedEvent DownValueButton => _downValueButton.onClick;
    public Button.ButtonClickedEvent CloseButton => _closeButton.onClick;

    private void Awake()
    {
        SetUiChild();
        _wallPropertyPresenter = _wallPropertyPresenterFactory.Create(this);
    }

    private void Start()
    {
        _wallPropertyPresenter.Initialize(_wall);
    }

    private void SetUiChild()
    {
        RectTransform tr = (RectTransform) transform;

        tr.SetParent(((AppView) _appView).transform);
        tr.anchoredPosition = Vector2.zero;
        tr.sizeDelta = Vector2.zero;
    }

    public void DestroyView()
    {
        Destroy(gameObject);
    }
}