public interface IWallPropertyPresenter
{
    void Initialize(Wall wall);
}