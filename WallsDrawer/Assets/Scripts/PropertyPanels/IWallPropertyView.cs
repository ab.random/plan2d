﻿using System;
using UnityEngine.UI;

public interface IWallPropertyView
{
    String Name { get; set; }
    float InputValue { get; set; }
    InputField.SubmitEvent EndInputValueEvent { get; }
    Button.ButtonClickedEvent UpValueButton { get; }
    Button.ButtonClickedEvent DownValueButton { get; }
    Button.ButtonClickedEvent CloseButton { get; }
    void DestroyView();
}