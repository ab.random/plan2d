using System.Globalization;
using UnityEngine;
using Zenject;

public class WallPropertyPresenter : IWallPropertyPresenter
{
    [Inject] private AppConfig _appConfig;
    [Inject] private IAppPresenter _appPresenter;

    private IWallPropertyView _wallPropertyView;
    private Wall _wall;

    public WallPropertyPresenter(IWallPropertyView wallPropertyView)
    {
        _wallPropertyView = wallPropertyView;
    }

    public void Initialize(Wall wall)
    {
        _wall = wall;
        
        _wallPropertyView.Name = nameof(Wall.Thickness);
        
        ChangeViewOfThickness(wall.Thickness);
        wall.OnThicknessChanged += ChangeViewOfThickness;
        
        _wallPropertyView.EndInputValueEvent.AddListener(CheckThicknessValue);
        _wallPropertyView.UpValueButton.AddListener(
            () => _wall.Thickness = ClampThickness(_wall.Thickness + _appConfig.StepThicknessChanging));
        _wallPropertyView.DownValueButton.AddListener(
            () => _wall.Thickness = ClampThickness(_wall.Thickness - _appConfig.StepThicknessChanging));
        
        _wallPropertyView.CloseButton.AddListener(Closer);
    }
    
    private void ChangeViewOfThickness(Wall wall)
    {
        ChangeViewOfThickness(wall.Thickness);
    }

    private void ChangeViewOfThickness(float thickness)
    {
        _wallPropertyView.InputValue = thickness;
    }

    private void CheckThicknessValue(string value)
    {
        if (float.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out var result))
        {
            _wall.Thickness = ClampThickness(result);
        }
        else
        {
            Debug.Log($"Bad value: {value}");
        }
    }

    private float ClampThickness(float thickness)
    {
        return Mathf.Clamp(thickness, _appConfig.MinThickness, _appConfig.MaxThickness);
    }

    private void Closer()
    {
         _wall.OnThicknessChanged -=  ChangeViewOfThickness;
         
         _wallPropertyView.EndInputValueEvent.RemoveAllListeners();
         _wallPropertyView.UpValueButton.RemoveAllListeners();
         _wallPropertyView.DownValueButton.RemoveAllListeners();
         _wallPropertyView.CloseButton.RemoveAllListeners();
         
         _appPresenter.Unselect();
    }
}