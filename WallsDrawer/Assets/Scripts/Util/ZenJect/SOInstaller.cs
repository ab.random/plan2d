using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "SOInstaller", menuName = "Project/SOInstaller")]
public class SOInstaller : ScriptableObjectInstaller<SOInstaller>
{
    [SerializeField] private AppConfig _appConfig;
    [SerializeField] private Tags _tags;
    [SerializeField] private MaterialStore _materialStore;
    [SerializeField] private UiPanels _uiPanels;

    public override void InstallBindings()
    {
        Container.BindInstance(_appConfig);
        Container.BindInstance(_tags);
        Container.BindInstance(_materialStore);
        Container.BindInstance(_uiPanels);
    }
}