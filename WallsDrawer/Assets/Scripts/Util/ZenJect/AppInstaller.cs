using UnityEngine;
using Zenject;

public class AppInstaller : MonoInstaller
{
    [Inject] private AppConfig _appConfig;
    [Inject] private UiPanels _uiPanels;

    public override void InstallBindings()
    {
        #region App

        Container.Bind<IAppModel>().To<AppModel>().AsSingle().NonLazy();
        Container.Bind<IAppPresenter>().To<AppPresenter>().AsSingle().NonLazy();
        Container.Bind<IAppView>().To<AppView>()
            .FromComponentInNewPrefab(_appConfig.Ui).WithGameObjectName("Ui").AsSingle().NonLazy();

        #endregion

        #region UI

        Container.Bind<BackgroundView>().FromComponentInNewPrefab(_appConfig.Background).WithGameObjectName("Background").AsSingle().NonLazy();
        Container.BindFactory<IBackgroundView, BackgroundPresenter, Factories.Ui.BackgroundPresenterFactory>().AsTransient();

        Container.BindFactory<WallPropertyView, WallPropertyPresenter, Factories.Ui.WallPropertyPresenterFactory>().AsTransient();
        Container.BindFactory<Wall, WallPropertyView, Factories.Ui.WallPropertyViewFactory>()
            .FromComponentInNewPrefab(_uiPanels.WallPropertyPanel).AsTransient();
        
        #endregion

        #region Points

        Container.BindFactory<float, float, Point, Factories.Points.PointFactory>().AsTransient();
        Container.BindFactory<Point, PointView, Factories.Points.PointViewFactory>()
            .FromComponentInNewPrefab(_appConfig.Point).AsTransient();
        Container.BindFactory<IPointView, PointPresenter, Factories.Points.PointPresenterFactory>().AsTransient();

        #endregion

        #region Walls

        Container.BindFactory<Point, Point, float, Wall, Factories.Walls.WallFactory>().AsTransient();
        Container.BindFactory<Wall, WallView, Factories.Walls.WallViewFactory>()
            .FromComponentInNewPrefab(_appConfig.Wall).AsTransient();
        Container.BindFactory<IWallView, WallPresenter, Factories.Walls.WallPresenterFactory>().AsTransient();

        #endregion
    }
}