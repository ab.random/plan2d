using System;
using System.Collections.Generic;
using UnityEngine;

public static class Helper
{
    #region Transform

    #region Line scale

    public static void ScaleX(this Transform tr, float scale)
    {
        var localScale = tr.localScale;
        localScale.x = scale;
        tr.localScale = localScale;
    }

    public static void ScaleY(this Transform tr, float scale)
    {
        var localScale = tr.localScale;
        localScale.y = scale;
        tr.localScale = localScale;
    }

    public static void ScaleZ(this Transform tr, float scale)
    {
        var localScale = tr.localScale;
        localScale.z = scale;
        tr.localScale = localScale;
    }

    #endregion

    #endregion

    #region Enumerable

    public static void ForEach<T>(this IEnumerable<T> elements, Action<T> action)
    {
        if (action == null) throw new ArgumentNullException();

        foreach (T element in elements)
        {
            action?.Invoke(element);
        }
    }

    #endregion
}