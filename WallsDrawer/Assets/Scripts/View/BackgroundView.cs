﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class BackgroundView : MonoBehaviour, IBackgroundView, IPointerClickHandler
{
    [Inject] private Factories.Ui.BackgroundPresenterFactory _backgroundPresenterFactory;

    private IBackgroundPresenter _backgroundPresenter;

    private void Awake()
    {
        _backgroundPresenter = _backgroundPresenterFactory.Create(this);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _backgroundPresenter.OnPointerClick(eventData);
    }
}