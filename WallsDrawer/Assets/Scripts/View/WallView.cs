﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

/// <summary>
/// By default, it's horizontal segment
/// </summary>
[RequireComponent(typeof(LineRenderer))]
public class WallView : MonoBehaviour, IWallView, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [HideInInspector] [SerializeField] private Transform _cachedTransform;
    [HideInInspector] [SerializeField] private LineRenderer _lineRenderer;

    [Inject] private Wall _wall;
    [Inject] private DrawCanvas _drawCanvas;
    [Inject] private MaterialStore _materialStore;
    [Inject] private Factories.Walls.WallPresenterFactory _presenterFactory;

    private IWallPresenter _wallPresenter;

    public Wall Wall => _wall;

    public Vector3 Position
    {
        get => _cachedTransform.position;
        set => _cachedTransform.position = value;
    }

    /// <summary>
    /// Angle of deviation from the horizontal (Ox)
    /// </summary>
    public float PlaneRotation
    {
        get => _cachedTransform.eulerAngles.z;
        set => _cachedTransform.Rotate(Vector3.forward, value, Space.World);
    }

    /// <summary>
    /// Need be exactly two points
    /// </summary>
    public Vector3[] WorldPoints
    {
        get
        {
            Vector3[] poss = new Vector3[_lineRenderer.positionCount];
            _lineRenderer.GetPositions(poss);
            return poss;
        }
        set => _lineRenderer.SetPositions(value);
    }

    /// <summary>
    /// Thickness of the renderer line and vertical scale
    /// </summary>
    public float Thickness
    {
        get => _cachedTransform.localScale.y;
        set
        {
            _lineRenderer.startWidth = value;
            _cachedTransform.ScaleY(value);
        }
    }

    /// <summary>
    /// Horizontal scale
    /// </summary>
    public float Length
    {
        get => _cachedTransform.localScale.x;
        set => _cachedTransform.ScaleX(value);
    }

    #region Editor

    private void OnValidate()
    {
        if (!_cachedTransform)
        {
            _cachedTransform = transform;
        }

        if (!_lineRenderer)
        {
            _lineRenderer = GetComponent<LineRenderer>();
        }
    }

    #endregion

    private void Awake()
    {
        _wallPresenter = _presenterFactory.Create(this);

        _cachedTransform.SetParent(_drawCanvas.CachedTransform);
    }

    private void Start()
    {
        _wallPresenter.Initialize(_wall);
    }

    public void Select(bool select)
    {
        _lineRenderer.material = select ? _materialStore.WallSelected : _materialStore.WallDefault;
    }

    public void Aimed(bool aimed)
    {
        _lineRenderer.material = aimed ? _materialStore.WallAimed : _materialStore.WallDefault;
    }

    #region UI Events

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!_wallPresenter.Selected)
        {
            Aimed(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!_wallPresenter.Selected)
        {
            Aimed(false);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _wallPresenter.OnPointerClick(eventData);
    }

    #endregion
}