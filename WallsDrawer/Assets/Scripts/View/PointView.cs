﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

[RequireComponent(typeof(Renderer))]
public class PointView : MonoBehaviour, IPointView, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler,
    IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [HideInInspector] [SerializeField] private Transform _cachedTransform;
    [HideInInspector] [SerializeField] private Renderer _renderer;

    [Inject] private Point _point;
    [Inject] private DrawCanvas _drawCanvas;
    [Inject] private MaterialStore _materialStore;
    [Inject] private Factories.Points.PointPresenterFactory _presenterFactory;

    private IPointPresenter _pointPresenter;
    private bool _new = true;

    public Point Point => _point;

    public Vector3 Position
    {
        get => _cachedTransform.position;
        set => _cachedTransform.position = value;
    }

    #region Editor

    private void OnValidate()
    {
        if (!_cachedTransform)
        {
            _cachedTransform = transform;
        }

        if (!_renderer)
        {
            _renderer = GetComponent<Renderer>();
        }
    }

    #endregion

    private void Awake()
    {
        _pointPresenter = _presenterFactory.Create(this);

        _cachedTransform.SetParent(_drawCanvas.CachedTransform);
    }

    private void Start()
    {
        _pointPresenter.Initialize(_point);
    }

    public void Aimed(bool aimed)
    {
        _renderer.material = aimed ? _materialStore.PointAimed : _materialStore.PointDefault;
    }

    #region UI Events

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!_new)
        {
            Aimed(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_new)
        {
            _new = false;
        }
        else
        {
            Aimed(false);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _pointPresenter.OnPointerClick(eventData);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _pointPresenter.OnBeginDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        _pointPresenter.OnDrag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _pointPresenter.OnEndDrag(eventData);
    }

    #endregion
}