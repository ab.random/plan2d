using UnityEngine;

public interface IWallView
{
    Wall Wall { get; }
    Vector3 Position { get; set; }
    float PlaneRotation { get; set; }
    Vector3[] WorldPoints { get; set; }
    float Thickness { get; set; }
    float Length { get; set; }
    void Select(bool select);
    void Aimed(bool aimed);
}