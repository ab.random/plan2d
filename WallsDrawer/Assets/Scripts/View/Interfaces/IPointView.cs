using UnityEngine;

public interface IPointView
{
    Point Point { get; }
    Vector3 Position { get; set; }
    void Aimed(bool aimed);
}