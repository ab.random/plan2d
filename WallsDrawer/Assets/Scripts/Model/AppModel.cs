﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

public class AppModel : IAppModel
{
    private List<Point> InternalPoints { get; } = new List<Point>();
    private List<Wall> InternalWalls { get; } = new List<Wall>();

    public ReadOnlyCollection<Point> Points => InternalPoints.AsReadOnly();
    public ReadOnlyCollection<Wall> Walls => InternalWalls.AsReadOnly();

    public void AddPoint(Point point)
    {
        InternalPoints.Add(point);
    }

    public void AddWall(Wall wall)
    {
        InternalWalls.Add(wall);
    }

    public bool CanPointReplaceTo(float newX, float newY)
    {
        return !InternalPoints.Any(point => point.X == newX && point.Y == newY);
    }

    /// <summary>
    /// We check that there are more points than one, and there is no wall with the last point added.
    /// </summary>
    /// <returns>true if can</returns>
    public bool CanWall()
    {
        return InternalPoints.Count > 1 && 
               !InternalWalls.SelectMany(wall => wall.Points).Contains(InternalPoints.Last());
    }

    /// <summary>
    /// Check than there are more walls than one, end the last wall contains no endPoint 
    /// </summary>
    /// <param name="endPoint"></param>
    /// <returns>true if can</returns>
    public bool CanLoopWall(Point endPoint)
    {
        return InternalWalls.Count > 1 && !InternalWalls.Last().Points.Contains(endPoint);
    }

    public void MakeLast(Point point)
    {
        InternalPoints.Remove(point);
        AddPoint(point);
    }
}