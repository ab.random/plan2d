﻿using System;

/// <summary>
/// The wall (segment) is defined by exactly two points.
/// </summary>
public class Wall
{
    public event Action<Wall> OnThicknessChanged;

    private float _thickness;

    public Point[] Points { get; }

    public float Thickness
    {
        get => _thickness;
        set
        {
            _thickness = value;
            OnThicknessChanged?.Invoke(this);
        }
    }

    public Wall(Point point0, Point point1, float thickness)
    {
        if (point0 == null || point1 == null)
        {
            throw new ArgumentException("Points should not be null");
        }

        Points = new[] {point0, point1};
        _thickness = thickness;
    }

    #region Overrides of object

    public override string ToString()
    {
        return $"{nameof(Wall)}. {nameof(Points)}: {Points[0]}, {Points[1]} {nameof(Thickness)}: {Thickness}";
    }

    protected bool Equals(Wall other)
    {
        return Points[0].Equals(other.Points[0]) && Points[1].Equals(other.Points[1]) ||
               Points[0].Equals(other.Points[1]) && Points[1].Equals(other.Points[0]);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((Wall) obj);
    }

    public override int GetHashCode()
    {
        return (Points != null ? Points.GetHashCode() : 0);
    }

    #endregion
}