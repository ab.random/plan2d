using System.Collections.ObjectModel;

public interface IAppModel
{
    ReadOnlyCollection<Point> Points { get; }
    ReadOnlyCollection<Wall> Walls { get; }
    void AddPoint(Point point);
    void AddWall(Wall wall);
    bool CanPointReplaceTo(float newX, float newY);
    bool CanWall();
    bool CanLoopWall(Point endPoint);
    void MakeLast(Point point);
}