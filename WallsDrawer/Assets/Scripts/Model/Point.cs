﻿using System;

public class Point
{
    public event Action<Point> OnPositionChanged;

    private float _x;
    private float _y;

    public float X
    {
        get => _x;
        set
        {
            _x = value;
            OnPositionChanged?.Invoke(this);
        }
    }

    public float Y
    {
        get => _y;
        set
        {
            _y = value;
            OnPositionChanged?.Invoke(this);
        }
    }

    public Point(float x, float y)
    {
        _x = x;
        _y = y;
    }

    public void SetNewPosition(float x, float y)
    {
        _x = x;
        _y = y;
        
        OnPositionChanged?.Invoke(this);
    }

    #region Overrides of object

    public override string ToString()
    {
        return $"{nameof(Point)}. {nameof(X)}: {X}, {nameof(Y)}: {Y}";
    }

    protected bool Equals(Point other)
    {
        return X.Equals(other.X) && Y.Equals(other.Y);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((Point) obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (X.GetHashCode() * 397) ^ Y.GetHashCode();
        }
    }

    #endregion
}